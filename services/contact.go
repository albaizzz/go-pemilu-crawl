package services

import "context"

type IWorkerSvc interface {
	Start()
	Stop(ctx context.Context) error
}
