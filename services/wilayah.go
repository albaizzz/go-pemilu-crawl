package services

import (
	"context"
	"fmt"
	"go-pemilu-crawl/models"
	"go-pemilu-crawl/repositories"
	"sync"

	"github.com/spf13/cast"
)

type WilayahService struct {
	kpurepository     repositories.IKPU
	wilayahRepository repositories.IWilayah
	shutdown          chan bool
}

func NewWilayahService(kpurepository repositories.IKPU, wilayahrepository repositories.IWilayah, shutdown chan bool) *WilayahService {
	wilayahSvc := &WilayahService{
		kpurepository:     kpurepository,
		wilayahRepository: wilayahrepository,
	}
	wilayahSvc.shutdown = shutdown
	return wilayahSvc
}

func (w *WilayahService) Start() {
	ctx := context.Background()
	for {
		select {
		case <-w.shutdown:
			return
		default:
			w.fetchWilayahParent(ctx, []string{"0"})
		}
	}
}

func (w *WilayahService) fetchWilayahParent(ctx context.Context, wilayahIds []string) error {
	wilayah, err := w.kpurepository.Wilayah(ctx, "wilayah", wilayahIds)
	if err != nil {
		fmt.Print(err)
	}
	wg := &sync.WaitGroup{}
	var wilayahParent models.Wilayah
	wilayahParent.WilayahID = cast.ToInt(1)
	wilayahParent.Nama = wilayah["1"].Nama
	wilayahParent.Dapil = wilayah["1"].Dapil
	wilayahParent.WilayahChild, err = w.fetchWilayah(ctx, wilayahParent.WilayahChild, []string{"1"})
	w.wilayahRepository.Save(ctx, wilayahParent)
	if err != nil {
		fmt.Println("error when save wilayah")
	}
	wg.Wait()
	w.shutdown <- true
	fmt.Println("DATA ALREADT SAVED")
	return nil
}

//

func (w *WilayahService) fetchWilayah(ctx context.Context, wilayahParent interface{}, wilayahIds []string) (*models.Wilayah, error) {
	wilayah, err := w.kpurepository.Wilayah(ctx, "wilayah", wilayahIds)
	if err != nil {
		fmt.Print(err)
	}
	for idx, _ := range wilayah {
		if len(idx) > 8 {
			return nil, nil
		}
		// go func(idx string) {
		if wilayahParent == nil {
			var wilayahModel models.Wilayah
			wilayahModel.WilayahID = cast.ToInt(idx)
			wilayahModel.Nama = wilayah[idx].Nama
			wilayahModel.Dapil = wilayah[idx].Dapil
			wilayahModel.WilayahChild, err = w.fetchWilayah(ctx, wilayahModel.WilayahChild, append(wilayahIds, idx))
		}
		if err != nil {
			fmt.Println("error when save wilayah")
		}
		// }(idx)
	}
	return nil, nil
}

func (p *WilayahService) Stop(ctx context.Context) error {
	p.shutdown <- true
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
			return nil
		}
	}
}
