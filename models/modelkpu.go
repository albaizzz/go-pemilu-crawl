package models

type Wilayahkpu struct {
	Nama  string `json:"nama"`
	Dapil []int  `json:"dapil"`
}

type InfoWilayahkpu map[string]Wilayahkpu

type WilayahDB struct {
	WilayahParent Wilayah `json:"wilayah"`
}
type Wilayah struct {
	WilayahID    int         `json:"wilayah_id"`
	Nama         string      `json:"nama"`
	Dapil        []int       `json:"dapil"`
	WilayahChild interface{} `json:"wilayahchild`
}

type SuaraTPS struct {
	SuaraPresiden SuaraPresiden `json:"chart"`
	Images        []string      `json:"images"`
	PemilihJ      int           `json:"pemilih_j"`
	PenggunaJ     int           `json:"pengguna_j"`
	SuaraSah      int           `json:"suara_sah"`
	SuaraTidakSah int           `json:"suara_tidak_sah"`
	SuaraTotal    int           `json:"suara_total"`
	Ts            string        `json:"ts"`
}

type SuaraPresiden struct {
	Jokowi  int `json:"21"`
	Prabowo int `json:"22"`
}
