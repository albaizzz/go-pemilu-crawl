package workers

type IFetcher interface {
	Fetch()
}
