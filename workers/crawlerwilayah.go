package workers

import (
	"go-pemilu-crawl/services"
)

type CrawlerWilayah struct {
	WorkerSvc services.IWorkerSvc
}

func NewCrawlerWilayah(workerSvc services.IWorkerSvc) *CrawlerWilayah {
	return &CrawlerWilayah{
		WorkerSvc: workerSvc,
	}
}

func (c *CrawlerWilayah) Fetch() {
	c.WorkerSvc.Start()
}
