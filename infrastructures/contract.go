package infrastructures

import "github.com/globalsign/mgo"

// MongoConnector is the interface of `MongoConnection`
type IMongo interface {
	CreateSession()
	GetConnection() *mgo.Session
	GetDBName() string
	Close()
}
