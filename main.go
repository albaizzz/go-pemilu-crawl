package main

import (
	"os"

	"go-pemilu-crawl/commands"

	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "Microservice crawl pemilu"
	app.Version = "1.0"
	app.Commands = []cli.Command{
		commands.Crawl,
	}
	app.Flags = append(app.Flags, []cli.Flag{}...)
	app.Run(os.Args)
}
