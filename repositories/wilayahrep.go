package repositories

import (
	"context"
	"go-pemilu-crawl/infrastructures"
	"go-pemilu-crawl/models"
)

type WilayahRepository struct {
	mgo infrastructures.IMongo
}

func NewWilayahRepository(mgo infrastructures.IMongo) *WilayahRepository {
	return &WilayahRepository{
		mgo: mgo,
	}
}

func (w *WilayahRepository) Save(ctx context.Context, WilayahDB models.Wilayah) error {
	db := w.mgo.GetConnection()

	err := db.DB(w.mgo.GetDBName()).C("wilayah").Insert(WilayahDB)

	if err != nil {
		return err
	}
	return nil
}
