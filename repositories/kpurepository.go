package repositories

import (
	"context"
	"encoding/json"
	"fmt"
	"go-pemilu-crawl/helpers"
	"go-pemilu-crawl/models"
	"io"
	"log"
	"net/http"
	"strings"

	config "github.com/spf13/viper"
)

type KpuRepository struct {
	BaseUrl string
}

func NewKpuRepository(BaseUrl string) *KpuRepository {
	return &KpuRepository{
		BaseUrl: BaseUrl,
	}
}

func (d *KpuRepository) Wilayah(ctx context.Context, category string, wilayahs []string) (models.InfoWilayahkpu, error) {

	url := fmt.Sprintf("%s%s/%s.json", d.BaseUrl, config.GetString(fmt.Sprintf("kpu.%s", category)), strings.Join(wilayahs, "/"))
	fmt.Println(url)
	response, err := helpers.ClientRequest(helpers.RequestOptions{
		URL:     url,
		Method:  http.MethodGet,
		Timeout: timeoutWhitelistValidator,
	})

	if err != nil {
		fmt.Print(err.Error())
		return nil, err
	}

	var wilayahKpu models.InfoWilayahkpu
	if response.HTTPStatus == http.StatusOK {

		dec := json.NewDecoder(strings.NewReader(string(response.Body)))
		for {
			if err := dec.Decode(&wilayahKpu); err == io.EOF {
				break
			} else if err != nil {
				log.Fatal(err)
			}
		}
	} else {
		return nil, fmt.Errorf("Failed fetch wilayah KPU")
	}
	return wilayahKpu, nil
}
