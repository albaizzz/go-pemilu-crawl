package repositories

import (
	"context"
	"go-pemilu-crawl/models"
)

const timeoutWhitelistValidator uint = 5000

type IKPU interface {
	Wilayah(ctx context.Context, category string, wilayahs []string) (models.InfoWilayahkpu, error)
}

type IWilayah interface {
	Save(ctx context.Context, wilayahDB models.Wilayah) error
}
