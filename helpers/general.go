package helpers

import (
	"crypto/sha256"
	"encoding/hex"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
)

// InArray check if an element is exist in the array
func InArray(val interface{}, array interface{}) (exists bool, index int) {
	exists = false
	index = -1
	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(array)
		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(val, s.Index(i).Interface()) {
				index = i
				exists = true
				return
			}
		}
	}
	return
}

// Sha256 hash with sha256
func Sha256(str string) string {
	data := []byte(str)
	temp := sha256.Sum256(data)
	return hex.EncodeToString(temp[:])
}

// // GenerateUUID generate UUID
// func GenerateUUID(withDash bool) string {
// 	u4 := uuid.NewV4()
// 	generatedUUID := u4.String()
// 	if !withDash {
// 		generatedUUID = strings.Replace(generatedUUID, "-", "", -1)
// 	}
// 	return generatedUUID
// }

// AbsolutePath is a helper function to get absolute path
func AbsolutePath(inPath string) string {
	if strings.HasPrefix(inPath, "$HOME") {
		inPath = userHomeDir() + inPath[5:]
	}

	if strings.HasPrefix(inPath, "$") {
		end := strings.Index(inPath, string(os.PathSeparator))
		inPath = os.Getenv(inPath[1:end]) + inPath[end:]
	}

	if filepath.IsAbs(inPath) {
		return filepath.Clean(inPath)
	}

	p, err := filepath.Abs(inPath)
	if err == nil {
		return filepath.Clean(p)
	}

	return ""
}

func userHomeDir() string {
	if runtime.GOOS == "windows" {
		home := os.Getenv("HOMEDRIVE") + os.Getenv("HOMEPATH")
		if home == "" {
			home = os.Getenv("USERPROFILE")
		}
		return home
	}
	return os.Getenv("HOME")
}

// ValidateName validate whether given name contains blocked words or not
func ValidateName(name string, blockedWords []string) (blockedWord string, isValid bool) {
	name = strings.ToLower(name)
	slicedName := strings.Split(name, " ")
	for _, word := range slicedName {
		isExist, index := InArray(word, blockedWords)
		if isExist {
			return blockedWords[index], false
		}
	}
	return "", true
}
