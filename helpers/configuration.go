package helpers

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	config "github.com/spf13/viper"
)

// LoadConfiguration is the function that used to load configuration file
func LoadConfiguration(path string) error {
	ext := filepath.Ext(path)
	ext = strings.TrimPrefix(ext, ".")
	config.SetConfigType(ext)

	file, err := os.Open(AbsolutePath(path))
	if err != nil {
		return err
	}
	defer file.Close()
	if err := config.ReadConfig(file); err != nil {
		return fmt.Errorf("%s: %s", "ERROR", err)
	}
	return nil
}
