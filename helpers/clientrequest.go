package helpers

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var defaultTimeout uint = 5000 // millisecond

type RequestOptions struct {
	Payload interface{}
	URL     string
	Header  map[string]string
	Method  string
	Timeout uint
}

// ClientResponse representation of http client response
type ClientResponse struct {
	HTTPStatus int
	Body       []byte
}

// client setup new client
var client = &http.Client{
	Transport: &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		Dial: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).Dial,
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
		TLSHandshakeTimeout:   10 * time.Second,
		ResponseHeaderTimeout: 10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
		MaxIdleConns:          100,
		MaxIdleConnsPerHost:   100,
	},
}

// ClientRequest http client request helper
func ClientRequest(option RequestOptions) (ClientResponse, error) {

	var rsp ClientResponse
	var err error
	var payload io.Reader

	// check option
	if option.Payload != nil {
		switch option.Payload.(type) {
		case url.Values:
			payload = strings.NewReader(option.Payload.(url.Values).Encode())
		case []byte:
			payload = bytes.NewBuffer(option.Payload.([]byte))
		case string:
			payload = strings.NewReader(option.Payload.(string))
		case io.Reader:
			payload = option.Payload.(io.Reader)
		case interface{}:
			b, err := json.Marshal(option.Payload)
			if err != nil {
				return rsp, err
			}
			payload = bytes.NewBuffer(b)

		default:
			err = fmt.Errorf("cannot handle type %T", option.Payload)
			return rsp, err
		}
	}

	// create request
	req, err := http.NewRequest(option.Method, option.URL, payload)
	if err != nil {
		rsp.HTTPStatus = http.StatusInternalServerError
		return rsp, err
	}

	// set headers
	for k, v := range option.Header {
		req.Header.Set(k, v)
	}

	if option.Timeout < 1 {
		option.Timeout = defaultTimeout
	}

	client.Timeout = time.Duration(option.Timeout) * time.Second
	result, err := client.Do(req)

	// check is time out
	if netErr, ok := err.(net.Error); ok {
		if netErr.Timeout() {
			rsp.HTTPStatus = http.StatusRequestTimeout
		}

		return rsp, fmt.Errorf("%v", err.Error())

	}

	// check err
	if err != nil {
		return rsp, fmt.Errorf("%v", err.Error())
	}

	rsp.HTTPStatus = result.StatusCode

	// read response body
	b, err := ioutil.ReadAll(result.Body)
	if result != nil {
		result.Body.Close()
	}
	if err != nil {
		return rsp, fmt.Errorf("%v", err.Error())
	}

	rsp.Body = b

	return rsp, err
}
