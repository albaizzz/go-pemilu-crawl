package commands

import (
	"go-pemilu-crawl/commands/actions"
	"go-pemilu-crawl/helpers"

	"github.com/urfave/cli"
)

var Crawl = cli.Command{
	Name:   "crawl",
	Action: actions.ServeCrawl,
	Flags: []cli.Flag{
		helpers.StringFlag("config, c", "configurations/App.yaml", "Custom configuration file path"),
	},
}
