package actions

import (
	"go-pemilu-crawl/helpers"
	"go-pemilu-crawl/infrastructures"
	"go-pemilu-crawl/services"
	"os"
	"os/signal"
	"syscall"
	"time"

	"go-pemilu-crawl/repositories"
	"go-pemilu-crawl/workers"

	log "github.com/sirupsen/logrus"
	config "github.com/spf13/viper"
	"github.com/urfave/cli"
)

func ServeCrawl(c *cli.Context) {

	if err := helpers.LoadConfiguration(c.String("config")); err != nil {
		log.Fatal("Error load configuration")
	}

	// graceful shutdown
	var signalShutdown = make(chan os.Signal)
	var paymentResponseSignal = make(chan bool)

	signal.Notify(signalShutdown, syscall.SIGTERM)
	signal.Notify(signalShutdown, syscall.SIGINT)

	go func() {
		sig := <-signalShutdown
		log.Infof("Caught sig: %+v in Consumer Topup Partner. Wait for 10 second to finish processing", sig)
		paymentResponseSignal <- true
		time.Sleep(10 * time.Second)
		os.Exit(0)
	}()

	crawlWilayah := InitCrawlerService(paymentResponseSignal)
	crawlWilayah.Fetch()
}

func InitCrawlerService(sigChan chan bool) *workers.CrawlerWilayah {
	kpurepository := repositories.NewKpuRepository(config.GetString("kpu.url"))
	mgo := InitMongo()
	wilayahRepository := repositories.NewWilayahRepository(mgo)
	wilayahSvc := services.NewWilayahService(kpurepository, wilayahRepository, sigChan)
	crawlWilayah := workers.NewCrawlerWilayah(wilayahSvc)
	return crawlWilayah
}

func InitMongo() *infrastructures.MongoConnection {
	mongo := new(infrastructures.MongoConnection)
	mongo.User = config.GetString("db.mongo.username")
	mongo.Password = config.GetString("db.mongo.password")
	mongo.Host = config.GetString("db.mongo.hosts")
	mongo.Port = config.GetInt("db.mongo.port")
	mongo.DBName = config.GetString("db.mongo.db_name")

	mongo.CreateSession()

	return mongo
}
